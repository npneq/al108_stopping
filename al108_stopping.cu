// © Alfredo Correa 2021

//find_package(PkgConfig REQUIRED)
//pkg_search_module(OpenBabel3 REQUIRED openbabel-3 IMPORTED_TARGET)
// target_link_libraries(${PROJECT_NAME} PkgConfig::OpenBabel3)

#include<openbabel/atom.h>
#include<openbabel/mol.h> // OpenBabel::OBMol
#include<openbabel/generic.h> // OpenBabel::OBUnitCell
#include<openbabel/obconversion.h>

#include<systems/electrons.hpp>
#include<systems/ions.hpp>

#include<operations/io.hpp> // io::save

#include<ground_state/calculate.hpp>
#include<ground_state/initialize.hpp>

//#include<real_time/propagate.hpp>
#include<real_time/propagator.hpp>

#include<mpi3/main.hpp>

#include<experimental/filesystem>

static auto to_inq_system_ions(OpenBabel::OBMol mol){
	static auto const A_over_b = 1.889726125; // angstrom over bohr

	namespace input = inq::input;

	input::cell const c = [&mol]{
		auto ouc = static_cast<OpenBabel::OBUnitCell const*>(mol.GetData(OpenBabel::OBGenericDataType::UnitCell));
		assert( ouc and ouc->GetAlpha() == 90. and ouc->GetBeta() == 90. and ouc->GetGamma() == 90. );
		using namespace inq::magnitude;
		return input::cell::orthorhombic(
			ouc->GetA()*A_over_b*1._b,
			ouc->GetB()*A_over_b*1._b,
			ouc->GetC()*A_over_b*1._b
		);
	}();

	std::vector<input::atom> const geo = [&mol]{
		std::vector<input::atom> geo;
		geo.reserve(mol.NumAtoms());
		std::transform(
			mol.BeginAtoms(), mol.EndAtoms(), std::back_inserter(geo), 
			[](auto e) -> input::atom{
				auto const p = e->GetVector()*A_over_b;
				return {
					input::species{pseudo::element{e->GetType()}}, 
					inq::math::vector3{p.x(), p.y(), p.z()} // in bohr
				};
			}
		);
		return geo;
	}();
	
	return inq::systems::ions{c, geo};
}

static auto to_OBMol(inq::systems::ions const& in){
	static auto const b_over_A = 1/1.889726125; // angstrom over bohr
	OpenBabel::OBMol out;

	for(std::size_t i = 0; i != in.geo().coordinates().size(); ++i){
		auto const Z = pseudo::element{in.geo().atoms()[i].symbol()}.atomic_number();
		auto const v = in.geo().coordinates()[i]*b_over_A;
		auto a = out.NewAtom();
		a->SetAtomicNum(Z);
		a->SetVector({v[0], v[1], v[2]});
	}

	auto const v0 = in.cell()[0]*b_over_A;
	auto const v1 = in.cell()[1]*b_over_A;
	auto const v2 = in.cell()[2]*b_over_A;

	OpenBabel::OBUnitCell uc;
	uc.SetData(
		{v0[0], v0[1], v0[2]}, 
		{v1[0], v1[1], v1[2]}, 
		{v2[0], v2[1], v2[2]}
	);
	out.CloneData(&uc);
	return out;
}

namespace mpi3 = boost::mpi3;

int mpi3::main(int argc, char* argv[], mpi3::communicator world){

	OpenBabel::OBMol mol;
	{
		OpenBabel::OBConversion obconv; obconv.SetInFormat("cif");
		obconv.ReadFile(&mol, "../al108.cif"); assert( mol.NumAtoms() == 108 );
	};
	{
		OpenBabel::OBConversion obconv; obconv.SetOutFormat("cif");
		obconv.WriteFile(&mol, "al108_read.cif");
	};

	inq::systems::ions ions = to_inq_system_ions(mol);
	ions.geo().add_atom(inq::input::species{pseudo::element{"H"}}, inq::math::vector3<double>{0.00000, 1.91325, 1.91325});
	
	{
		auto mol2 = to_OBMol(ions);
		OpenBabel::OBConversion obconv; obconv.SetOutFormat("cif");
		obconv.WriteFile(&mol2, "al108_H.cif");
	}

	using namespace inq::magnitude;

	inq::systems::electrons electrons(
		world, ions, 
		inq::input::basis::cutoff_energy(25.0_Ha),
		inq::input::config{
			.extra_states = 32,
			.excess_charge = -1.,
			.temperature = 300.0_K
		}
	);

	inq::ground_state::initialize(ions, electrons);

	namespace filesystem = std::experimental::filesystem;
	if(filesystem::exists("al108_restart")) inq::operations::io::load("al108_restart", electrons.phi_);

	auto result = inq::ground_state::calculate(
		ions, electrons, inq::input::interaction::pbe(),
			inq::input::scf::steepest_descent() |
			inq::input::scf::energy_tolerance(1E-5_Ha) |
			inq::input::scf::scf_steps(200)
	);

	assert( argc > 1 );
	double const v = std::atof(argv[1]);
	assert( v > 0. );
	std::cout<<"v = "<< v <<std::endl;

	inq::operations::io::save("al108_restart"+std::string(argv[1]), electrons.phi_);
	filesystem::remove_all("al108_restart");
	filesystem::rename("al108_restart"+std::string(argv[1]), "al108_restart");

	auto const dx = 0.003;
	auto const dt = std::min(0.030, dx/v)*1._atomictime;

	ions.geo().velocities().back() = inq::math::vector3<double>(v, 0.0, 0.0);

	std::ofstream ofs;
	if(world.root()) ofs.open("al108_td_v"+std::to_string(v)+".dat");
	if(ofs) ofs<<"# distance (au), energy (au)\n";

	inq::real_time::propagator<inq::ions::propagator::impulsive> p(ions, electrons, inq::input::interaction::pbe(), {});
	while(ions.geo().coordinates().back()[0] < ions.cell()[0][0]){
		p.do_step(dt);
		if(ofs){
			ofs 
				<< ions.geo().coordinates().back()[0] <<'\t'
				<< p.energy().total()
			<<std::endl;
		}
	}

	return 0;
}

